class Link < ApplicationRecord
    # owning all of the logic in the method so the UI can be dumb
    # wanting to make sure we can make sure there's a URL
    validates_presence_of :url
    # is there http or https?
    validates :url, format: URI::regexp(%w[http https])
    # is the slug different than what we've seen
    validates_uniqueness_of :slug

    before_validation :generate_slug

    def short
        Rails.application.routes.url_helpers.short_url(slug: self.slug)
    end

    def generate_slug
        self.slug = SecureRandom.uuid[0..5] if self.slug.nil? || self.slug.empty?
        true
    end

    def self.shorten(url, slug = '')
        # return short when URL with that slug was created before
        link = Link.where(url: url, slug: slug).first
        return link.short if link 
          
        # create a new
        link = Link.new(url: url, slug: slug)
        return link.short if link.save
          
        # if slug is taken, try to add random characters
        Link.shorten(url, slug + SecureRandom.uuid[0..2])
    end

end

