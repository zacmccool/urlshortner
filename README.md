# README
Quick app to shorten up a url and test out some stuff

Key versions needed to run this app:

* Ruby version  
   2.5.3

* System dependencies  
   rails '~> 5.2.4', '>= 5.2.4.1'

* Database creation  
   sqlite3

Look in the gemfile for any other compatibility questions.

# Deployment instructions
   bundle install  
   rake db:migrate  
   rails s
   localhost:3000/links
