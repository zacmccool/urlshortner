Rails.application.routes.draw do
  default_url_options :host => "example.com"
  get '/s/:slug', to: 'links#shortRedirect', as: :short
  get '/', to: 'links#new'
  resources :links do
    collection do
      get :shortRedirect 
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
